ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .gitignore
* .idea/
* .idea/.name
* .idea/compiler.xml
* .idea/copyright/
* .idea/copyright/profiles_settings.xml
* .idea/encodings.xml
* .idea/misc.xml
* .idea/modules.xml
* .idea/scopes/
* .idea/scopes/scope_settings.xml
* .idea/vcs.xml
* .idea/workspace.xml
* git/
* git/HEAD
* git/config
* git/description
* git/hooks/
* git/hooks/applypatch-msg.sample
* git/hooks/commit-msg.sample
* git/hooks/post-update.sample
* git/hooks/pre-applypatch.sample
* git/hooks/pre-commit.sample
* git/hooks/pre-push.sample
* git/hooks/pre-rebase.sample
* git/hooks/prepare-commit-msg.sample
* git/hooks/update.sample
* git/index
* git/info/
* git/info/exclude
* git/logs/
* git/logs/HEAD
* git/logs/refs/
* git/logs/refs/heads/
* git/logs/refs/heads/master
* git/logs/refs/remotes/
* git/logs/refs/remotes/origin/
* git/logs/refs/remotes/origin/HEAD
* git/objects/
* git/objects/01/
* git/objects/01/93c2673203020e3726e397a6870107f9e38aa9
* git/objects/02/
* git/objects/02/f9c4272febd3648ea99934446cd84f70081678
* git/objects/0d/
* git/objects/0d/6800034b898bdbf9c0cf4f841148fcc7ad6ae8
* git/objects/16/
* git/objects/16/21825f66b4f936cabd3baf580469c13038a677
* git/objects/1f/
* git/objects/1f/1f8f4b96e6c86bc3337702205cc39ebbbc0eef
* git/objects/20/
* git/objects/20/3cd4e9e29d4b7cff53169168cc02286cceeafa
* git/objects/21/
* git/objects/21/7af471a9e60576e8c36373fa22c50a63a00b2c
* git/objects/27/
* git/objects/27/5077f82558f02f5c5151cc5c676b9f5f60dce3
* git/objects/28/
* git/objects/28/8b66551d1efd1f13dd06f20a67534d2df57946
* git/objects/2b/
* git/objects/2b/529fc16e96dd3a918028e44d875bddc31542be
* git/objects/3c/
* git/objects/3c/02242ad044be9b8c7c09e7c90c5d427763fe97
* git/objects/3c/79ab3edd60744eef04d5e8b12622acebebc78c
* git/objects/44/
* git/objects/44/f01db75f0fef18081132a9e86517f8d5efa8f6
* git/objects/4a/
* git/objects/4a/99ca196a3d1e104ae788fdcb00b5344bc82478
* git/objects/54/
* git/objects/54/31340ffe0643b09c777fec426b69ee5e7330d0
* git/objects/55/
* git/objects/55/c1e5908c7e0f157fe815acd6d5cd7358463390
* git/objects/55/c5472db4a9d28ddd3f54c065bbd95273567570
* git/objects/61/
* git/objects/61/e3fa8fbca01c469f05fb488bdb8f5ffdb9da3c
* git/objects/63/
* git/objects/63/118b945db5de59741fdcda0cf22f09d9b05ab5
* git/objects/68/
* git/objects/68/cddf08b2e717946e459ce5e52431212c099aae
* git/objects/6a/
* git/objects/6a/e570b4db4da165fada0650079061cb56aa8793
* git/objects/6c/
* git/objects/6c/e89c7ba4394d8cab27953d41456ce234ca37c3
* git/objects/74/
* git/objects/74/68c6f2458bfeb0a8e92b706346d05d229d8692
* git/objects/7a/
* git/objects/7a/73bc8b224ca953d3169defc36dd975fb075cb1
* git/objects/7b/
* git/objects/7b/c01d9a9c6873b7e4fea3b29ee945267845ae86
* git/objects/80/
* git/objects/80/63403b4205d3854549c887b803e76ecb754848
* git/objects/82/
* git/objects/82/68fd15ab91333f44990cfafb7394c3849d8c94
* git/objects/82/efe74e4f6532c0e162674955f65ccf0f381387
* git/objects/83/
* git/objects/83/83ba66cb877e173553f6bdbabbfb9a13132b9d
* git/objects/85/
* git/objects/85/a6081587e2c2b9793d796ee7b07e12fdf860db
* git/objects/86/
* git/objects/86/8b727072aed9fd582deec519cc59e04ca005ec
* git/objects/92/
* git/objects/92/2003b8433bcad6ce9778a37628d738faa26389
* git/objects/94/
* git/objects/94/383a5114ebfbbc2aa70c55202ce8094c7b6d36
* git/objects/9e/
* git/objects/9e/b5085974e8c039c75fb82d47b67e5a3e247e61
* git/objects/a0/
* git/objects/a0/4e201056fa669386c9794916b2be0bdd83aa04
* git/objects/a1/
* git/objects/a1/8cbb48c431dfcc163fce2b6e27e981f8c09f6a
* git/objects/a6/
* git/objects/a6/5e947629af37c0ee480b319e12c842a5a540ca
* git/objects/a9/
* git/objects/a9/1fd0372b20f85e284fc2fa2ce949176dfdf6e5
* git/objects/bb/
* git/objects/bb/7e759cdd063b8627fcffd0f6643d2b887c6178
* git/objects/c0/
* git/objects/c0/0202823049e8d5b1590e1223f20fef8a966353
* git/objects/c3/
* git/objects/c3/89e3241117e206b1a7c59648b8a1ff25a99227
* git/objects/c9/
* git/objects/c9/d074ee614bbf9749627845448590056b2a3fd1
* git/objects/cc/
* git/objects/cc/7b42ff0083a7e384150181fc570bbb88309873
* git/objects/ce/
* git/objects/ce/39f2d0a061116c8dfd41e74b2bfd54ff4d554b
* git/objects/cf/
* git/objects/cf/12d2839dc5ffe618b0233f0c30e681e8b9b0f2
* git/objects/d3/
* git/objects/d3/42bedcd77ef90f6fecb2a08f303284ddbab01a
* git/objects/d4/
* git/objects/d4/fb7cd9d868f1d7d9964f1686dcbc018ef9495a
* git/objects/d5/
* git/objects/d5/b8db743bc91f1428770460e628d636fe3184bf
* git/objects/e2/
* git/objects/e2/06d70d8595e2a50675ba11de48efcfa012497d
* git/objects/e7/
* git/objects/e7/bedf3377d40335424fd605124d4761390218bb
* git/objects/f2/
* git/objects/f2/fe1559a217865a5454add526dcc446f892385b
* git/objects/f3/
* git/objects/f3/8ba98d8c95c0502fa51bcf1890245888186ddc
* git/objects/f4/
* git/objects/f4/17f06cff6518835b49066dae5dd9f0a43fbdc2
* git/objects/f4/4de220130a24ae7f60d77e7985b3286cf4b93e
* git/objects/f8/
* git/objects/f8/0ea04c083c2bd0aa4b3a791596f7d2d59c8c36
* git/objects/fb/
* git/objects/fb/5c0708a7019786631346fe164dda01e3c3c127
* git/objects/fd/
* git/objects/fd/668c170d249bad2ace06288e7ba58a427597d8
* git/packed-refs
* git/refs/
* git/refs/heads/
* git/refs/heads/master
* git/refs/remotes/
* git/refs/remotes/origin/
* git/refs/remotes/origin/HEAD
* ic_launcher-web.png
* proguard-project.txt

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:18.0.0

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* res/ => app/src/main/res/
* src/ => app/src/main/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
